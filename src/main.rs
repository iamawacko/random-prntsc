use rand::prelude::*;

fn main() {
    const LOWERCASE: [char; 26] = [
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
        's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
    ];

    let mut rng = rand::thread_rng();
    let letter = LOWERCASE.choose(&mut rng).unwrap();
    let other_letter = LOWERCASE.choose(&mut rng).unwrap();
    let digits = rng.gen_range(0..9999);

    println!("https://prnt.sc/{}{}{}", letter, other_letter, digits);
}
